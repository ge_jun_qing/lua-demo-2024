local paymode = KEYS[1]
local signnum = KEYS[2]
local limitday = KEYS[3]
local limitmonth = string.sub(limitday,1,6)

local amount = tonumber(ARGV[1])
local daycount = tonumber(ARGV[2])
local daynormamount = tonumber(ARGV[3])
local monthnormamount = tonumber(ARGV[4])
local time = tonumber(ARGV[5])

local paylimitcountkey = 'paylimit:' .. paymode .. ':' .. signnum .. '_day_count_' .. limitday
local paylimitdaynormkey = 'paylimit:' .. paymode .. ':' .. signnum .. '_day_norm_' .. limitday
local paylimitmonthnormkey = 'paylimit:' .. paymode .. ':' .. signnum .. '_month_norm_' .. limitmonth

local paylimitcountnumber = 0
local paylimitdaynormnumber = 0
local paylimitmonthnormnumber = 0

local paylimitcount = redis.call('GET', paylimitcountkey)
local paylimitdaynorm = redis.call('GET', paylimitdaynormkey)
local paylimitmonthnorm = redis.call('GET', paylimitmonthnormkey)

if (not paylimitcount) then paylimitcountnumber = 0 else paylimitcountnumber=tonumber(paylimitcount) end
if (not paylimitdaynorm) then paylimitdaynormnumber = 0 else paylimitdaynormnumber=tonumber(paylimitdaynorm) end
if (not paylimitmonthnorm) then paylimitmonthnormnumber = 0 else paylimitmonthnormnumber=tonumber(paylimitmonthnorm) end

if(daycount ~= -1 and (paylimitcountnumber + 1) > daycount) then return 1 end
if(daynormamount ~= -1 and (paylimitdaynormnumber + amount) > daynormamount) then return 2 end
if(monthnormamount ~= -1 and (paylimitmonthnormnumber + amount) > monthnormamount) then return 3 end

local countres = redis.call('INCRBY',paylimitcountkey,1)
if(tonumber(countres) == 1) then redis.call('EXPIRE',paylimitcountkey,time) end
local dayamountres = redis.call('INCRBY',paylimitdaynormkey,amount)
if(tonumber(dayamountres) == amount) then redis.call('EXPIRE',paylimitdaynormkey,time) end
local monthamountres = redis.call('INCRBY',paylimitmonthnormkey,amount)
if(tonumber(monthamountres) == amount) then redis.call('EXPIRE',paylimitmonthnormkey,time) end

return 0