local transcode = KEYS[1]
local signnum = KEYS[2]
local limitday = KEYS[3]
local limitmonth = string.sub(limitday,1,6)

local amount = tonumber(ARGV[1])
local daycount = tonumber(ARGV[2])
local daynormamount = tonumber(ARGV[3])
local monthnormamount = tonumber(ARGV[4])
local time = tonumber(ARGV[5])

local orderlimitcountkey = 'orderlimit:' .. transcode .. ':' .. signnum .. '_day_count_' .. limitday
local orderlimitdaynormkey = 'orderlimit:' .. transcode .. ':' .. signnum .. '_day_norm_' .. limitday
local orderlimitmonthnormkey = 'orderlimit:' .. transcode .. ':' .. signnum .. '_month_norm_' .. limitmonth

local orderlimitcountnumber = 0
local orderlimitdaynormnumber = 0
local orderlimitmonthnormnumber = 0

local orderlimitcount = redis.call('GET', orderlimitcountkey)
local orderlimitdaynorm = redis.call('GET', orderlimitdaynormkey)
local orderlimitmonthnorm = redis.call('GET', orderlimitmonthnormkey)

if (not orderlimitcount) then orderlimitcountnumber = 0 else orderlimitcountnumber=tonumber(orderlimitcount) end
if (not orderlimitdaynorm) then orderlimitdaynormnumber = 0 else orderlimitdaynormnumber=tonumber(orderlimitdaynorm) end
if (not orderlimitmonthnorm) then orderlimitmonthnormnumber = 0 else orderlimitmonthnormnumber=tonumber(orderlimitmonthnorm) end

if(daycount ~= -1 and (orderlimitcountnumber + 1) > daycount) then return 1 end
if(daynormamount ~= -1 and (orderlimitdaynormnumber + amount) > daynormamount) then return 2 end
if(monthnormamount ~= -1 and (orderlimitmonthnormnumber + amount) > monthnormamount) then return 3 end

local countres = redis.call('INCRBY',orderlimitcountkey,1)
if(tonumber(countres) == 1) then redis.call('EXPIRE',orderlimitcountkey,time) end
local dayamountres = redis.call('INCRBY',orderlimitdaynormkey,amount)
if(tonumber(dayamountres) == amount) then redis.call('EXPIRE',orderlimitdaynormkey,time) end
local monthamountres = redis.call('INCRBY',orderlimitmonthnormkey,amount)
if(tonumber(monthamountres) == amount) then redis.call('EXPIRE',orderlimitmonthnormkey,time) end

return 0