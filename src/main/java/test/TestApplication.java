/**
 * Copyright (C), 2015-2024, 通联支付网络股份有限公司
 * FileName: TestApplication
 * Author:   gejunqing
 * Date:     2024/1/13 下午4:21
 * Description: test
 * History:
 * <author>          <time>          <version>          <desc>
 * 作者姓名           修改时间           版本号              描述
 */
package test;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * 〈一句话功能简述〉<br> 
 * 〈test〉
 *
 * @author gejunqing
 * @create 2024/1/13
 * @since 1.0.0
 */
@RestController
@SpringBootApplication
public class TestApplication extends SpringBootServletInitializer
{
    private Logger logger = LoggerFactory.getLogger(TestApplication.class);
    @Autowired
    private TestService testService;

    public static void main(String[] args)
    {
        SpringApplication.run(TestApplication.class, args);
    }

    @RequestMapping("/")
    public String test00()
    {
        return "hello world!!!!!";
    }

    @RequestMapping("/test0")
    public String test0()
    {
        return "hello world test0";
    }

    @RequestMapping("/test")
    public String test(@RequestParam(name = "num",required = false) String num)
    {
        long b1 = System.currentTimeMillis();

        for (int i = 0; i < 120; i++)
        {
            long b = System.currentTimeMillis();
            String signnum = StringUtils.rightPad(i+"",8,"0");
            String s = testService.testLua(StringUtils.isBlank(num)?signnum:num);
            long e = System.currentTimeMillis();
            logger.info("耗时:{}-ms,结果:{}",(e-b),s);

        }
        long e1 = System.currentTimeMillis();

        return "hello world,总耗时:"+(e1-b1)+" ms";
    }

    @RequestMapping("/test1")
    public String test1()
    {
        return "hello world test1";
    }

    @RequestMapping("/test2")
    public String test2(@RequestParam(name = "num",required = false) String num)
    {
        long b1 = System.currentTimeMillis();

        for (int i = 0; i < 120; i++)
        {
            long b = System.currentTimeMillis();
            String signnum = StringUtils.rightPad(i+"",8,"0");
            String s = testService.testLua2(StringUtils.isBlank(num)?signnum:num);
            long e = System.currentTimeMillis();
            logger.info("耗时:{}-ms,结果:{}",(e-b),s);

        }
        long e1 = System.currentTimeMillis();

        return "hello world2,总耗时:"+(e1-b1)+" ms";
    }
}