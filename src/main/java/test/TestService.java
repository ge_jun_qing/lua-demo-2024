/**
 * Copyright (C), 2015-2024, 通联支付网络股份有限公司
 * FileName: TestService
 * Author:   gejunqing
 * Date:     2024/1/13 下午4:28
 * Description: testService
 * History:
 * <author>          <time>          <version>          <desc>
 * 作者姓名           修改时间           版本号              描述
 */
package test;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.data.redis.core.script.DefaultRedisScript;
import org.springframework.scripting.support.ResourceScriptSource;
import org.springframework.stereotype.Service;

/**
 * 〈一句话功能简述〉<br> 
 * 〈testService〉
 *
 * @author gejunqing
 * @create 2024/1/13
 * @since 1.0.0
 */
@Service
public class TestService
{
    private Logger logger = LoggerFactory.getLogger(TestService.class);
    private DefaultRedisScript<Long> payLimitRedisScript;
    private DefaultRedisScript<Long> orderLimitRedisScript;
    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    @PostConstruct
    public void init()
    {
        payLimitRedisScript = new DefaultRedisScript<Long>();
        payLimitRedisScript.setResultType(Long.class);
        payLimitRedisScript.setScriptSource(new ResourceScriptSource(new ClassPathResource("paylimit.lua")));

        orderLimitRedisScript = new DefaultRedisScript<Long>();
        orderLimitRedisScript.setResultType(Long.class);
        orderLimitRedisScript.setScriptSource(new ResourceScriptSource(new ClassPathResource("orderlimit.lua")));
    }

    public String testLua (String signnum)
    {
        try
        {
            /**
             * List设置lua的KEYS
             */
            List<String> keyList = new ArrayList<>();
            keyList.add("WEIXIN");
            keyList.add(signnum);
            keyList.add("20240113");
            keyList.add("202401");
            /**
             * 调用脚本并执行
             */
            Long i = stringRedisTemplate.execute(payLimitRedisScript, keyList, "1", "100", "120","-1","600");

            return i.toString();
        }
        catch (Exception e)
        {
            logger.error("执行lua脚本异常", e);
            return "1";
        }
    }

    public String testLua2 (String signnum)
    {
        try
        {
            /**
             * List设置lua的KEYS
             */
            List<String> keyList = new ArrayList<>();
            keyList.add("2080");
            keyList.add(signnum);
            keyList.add("20240113");
            keyList.add("202401");
            /**
             * 调用脚本并执行
             */
            Long i = stringRedisTemplate.execute(orderLimitRedisScript, keyList, "1", "100", "120","-1","600");

            return i.toString();
        }
        catch (Exception e)
        {
            logger.error("执行lua脚本异常", e);
            return "1";
        }
    }
}