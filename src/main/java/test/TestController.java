/**
 * Copyright (C), 2015-2024, 通联支付网络股份有限公司
 * FileName: TestController
 * Author:   gejunqing
 * Date:     2024/1/13 下午8:43
 * Description: ctrl
 * History:
 * <author>          <time>          <version>          <desc>
 * 作者姓名           修改时间           版本号              描述
 */
package test;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 〈一句话功能简述〉<br> 
 * 〈ctrl〉
 *
 * @author gejunqing
 * @create 2024/1/13
 * @since 1.0.0
 */
@RestController
public class TestController
{
    @RequestMapping("/tests")
    public String tests()
    {
        return "hello world!!!!!";
    }
}